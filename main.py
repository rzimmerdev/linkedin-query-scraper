import json, os, time
from linkedin_scraper import Person, actions
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

email = ""
password = ""
query = ""
total_pages = 1
profiles = []
current_page = 1

def get_links(driver, profiles):
	for x in driver.find_elements_by_class_name("entity-result__title-text"):
		profiles.append(x.find_element_by_css_selector("*").get_attribute("href"))
	return profiles

def get_profiles(driver, profiles):
	return 0

with open("config.json") as conf:
	config = json.loads(conf.read())
	email = config["email"]
	password = config["password"]
	query = config["query"]
	total_pages = int(config["total_pages"])

if os.path.isfile(config["output_file"] + "_links.json"):
	links = []
	profiles = []
	with open(config["output_file"] + "_links.json", 'r') as file:
		links = json.loads(file.read())
	for url in links:
		driver = webdriver.Chrome()
		actions.login(driver, email, password)
		person = Person(linkedin_url=url, driver=driver)
		profiles.append(
			{
				"name": person.name,
				"about": person.about,
				"experiences": [str(x) for x in person.experiences],
				"educations": [str(x) for x in person.educations],
				"interests": [str(x) for x in person.interests],
				
				"company": person.company,
				"job_title": person.job_title
			}
		)
	with open(config["output_file"] + "_profiles.json", 'w') as file:
		file.write(json.dumps(profiles, indent=4))

else:
	driver = webdriver.Chrome()
	actions.login(driver, email, password)
	driver.get(query)
	while current_page <= total_pages:
		WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.CLASS_NAME, "search-results-page"))
		)
	
		#find_element_by_class_name("search-results-page").send_keys(Keys.END)
		current_page += 1
		driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		#element = driver.find_element_by_class_name("artdeco-pagination__button--next")
		#element.send_keys(Keys.END)
		WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.CLASS_NAME, "artdeco-pagination__button--next"))
		)
		profiles = get_links(driver, profiles)
		driver.find_element_by_class_name("artdeco-pagination__button--next").click()

	with open(config["output_file" + "_links.json"], 'w') as file:
		file.write(json.dumps(profiles))

	

driver.quit()

#person = Person("https://www.linkedin.com/in/andre-iguodala-65b48ab5", driver=driver)
#print(person)
